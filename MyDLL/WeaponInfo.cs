﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MyDLL
{
    public partial class WeaponInfo
    {
        [JsonProperty("Response")]
        public ResponseWeaponInfo Response { get; set; }
    }

    public partial class ResponseWeaponInfo
    {
        [JsonProperty("Type")]
        public string Type { get; set; }

        [JsonProperty("ItemsInfo")]
        public WeaponsInfo[] ItemsInfo { get; set; }
    }

    public partial class WeaponsInfo
    {
        [JsonProperty("UnicKey")]
        public string UnicKey { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }
    }
}
