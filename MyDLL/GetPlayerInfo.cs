﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MyDLL
{
    public partial class GetPlayerInfo
    {
        [JsonProperty("response")]
        public ResponseGetPlayerInfo Response { get; set; }
    }

    public partial class ResponseGetPlayerInfo
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("Sex")]
        public long Sex { get; set; }

        [JsonProperty("IsCreatedCharacter")]
        public bool IsCreatedCharacter { get; set; }

        [JsonProperty("VisualInformation")]
        public VisualInformationGetPlayerInfo VisualInformation { get; set; }

        [JsonProperty("itemsId")]
        public ItemsIdGetPlayerInfo[] ItemsId { get; set; }
    }

    public partial class ItemsIdGetPlayerInfo
    {
        [JsonProperty("id")]
        public string Id { get; set; }
    }

    public partial class VisualInformationGetPlayerInfo
    {
        [JsonProperty("ColorBody")]
        public string ColorBody { get; set; }

        [JsonProperty("ColorEye")]
        public string ColorEye { get; set; }

        [JsonProperty("ColorHairFront_1")]
        public string ColorHairFront1 { get; set; }

        [JsonProperty("ColorHairFront_2")]
        public string ColorHairFront2 { get; set; }

        [JsonProperty("ColorHairBack_1")]
        public string ColorHairBack1 { get; set; }

        [JsonProperty("ColorHairBack_2")]
        public string ColorHairBack2 { get; set; }

        [JsonProperty("ColorTail_1")]
        public string ColorTail1 { get; set; }

        [JsonProperty("ColorTail_2")]
        public string ColorTail2 { get; set; }

        [JsonProperty("ModelHairFront")]
        public string ModelHairFront { get; set; }

        [JsonProperty("ModelHairBack")]
        public string ModelHairBack { get; set; }

        [JsonProperty("ModelTail")]
        public string ModelTail { get; set; }
    }
}
