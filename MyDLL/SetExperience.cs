﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace MyDLL
{
    public partial class SetExperience
    {
        [JsonProperty("Response")]
        public ResponseSetExperience Response { get; set; }
    }

    public partial class ResponseSetExperience
    {
        [JsonProperty("Type")]
        public string Type { get; set; }

        [JsonProperty("ExperiencePoint")]
        public long ExperiencePoint { get; set; }
    }
}
