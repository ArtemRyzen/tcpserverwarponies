﻿namespace TCPServerForWarPonies_WF
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.TB_DataServer = new System.Windows.Forms.TabPage();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.B_ClearLogs = new System.Windows.Forms.Button();
            this.B_SaveLogs = new System.Windows.Forms.Button();
            this.TB_PortDataServer = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.B_ServerStart = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.RTB_Logs = new System.Windows.Forms.RichTextBox();
            this.TB_MasterServer = new System.Windows.Forms.TabPage();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.B_RunGameServer = new System.Windows.Forms.Button();
            this.L_CountRunServer = new System.Windows.Forms.Label();
            this.RTB_LogsMS = new System.Windows.Forms.RichTextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.TB_DataServer.SuspendLayout();
            this.TB_MasterServer.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.tabControl1.Controls.Add(this.TB_DataServer);
            this.tabControl1.Controls.Add(this.TB_MasterServer);
            this.tabControl1.Location = new System.Drawing.Point(1, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(769, 444);
            this.tabControl1.TabIndex = 0;
            // 
            // TB_DataServer
            // 
            this.TB_DataServer.Controls.Add(this.label2);
            this.TB_DataServer.Controls.Add(this.button2);
            this.TB_DataServer.Controls.Add(this.B_ClearLogs);
            this.TB_DataServer.Controls.Add(this.B_SaveLogs);
            this.TB_DataServer.Controls.Add(this.TB_PortDataServer);
            this.TB_DataServer.Controls.Add(this.button1);
            this.TB_DataServer.Controls.Add(this.B_ServerStart);
            this.TB_DataServer.Controls.Add(this.label1);
            this.TB_DataServer.Controls.Add(this.RTB_Logs);
            this.TB_DataServer.Location = new System.Drawing.Point(4, 22);
            this.TB_DataServer.Name = "TB_DataServer";
            this.TB_DataServer.Padding = new System.Windows.Forms.Padding(3);
            this.TB_DataServer.Size = new System.Drawing.Size(761, 418);
            this.TB_DataServer.TabIndex = 0;
            this.TB_DataServer.Text = "Data-Server";
            this.TB_DataServer.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(637, 158);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Порт запуска сервера";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(640, 66);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(110, 34);
            this.button2.TabIndex = 12;
            this.button2.Text = "Перезапустить сервер";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // B_ClearLogs
            // 
            this.B_ClearLogs.Location = new System.Drawing.Point(640, 231);
            this.B_ClearLogs.Name = "B_ClearLogs";
            this.B_ClearLogs.Size = new System.Drawing.Size(110, 23);
            this.B_ClearLogs.TabIndex = 11;
            this.B_ClearLogs.Text = "Отчистить логи";
            this.B_ClearLogs.UseVisualStyleBackColor = true;
            this.B_ClearLogs.Click += new System.EventHandler(this.B_ClearLogs_Click);
            // 
            // B_SaveLogs
            // 
            this.B_SaveLogs.Location = new System.Drawing.Point(640, 201);
            this.B_SaveLogs.Name = "B_SaveLogs";
            this.B_SaveLogs.Size = new System.Drawing.Size(110, 23);
            this.B_SaveLogs.TabIndex = 10;
            this.B_SaveLogs.Text = "Сохранить логи";
            this.B_SaveLogs.UseVisualStyleBackColor = true;
            this.B_SaveLogs.Click += new System.EventHandler(this.B_SaveLogs_Click);
            // 
            // TB_PortDataServer
            // 
            this.TB_PortDataServer.Location = new System.Drawing.Point(640, 174);
            this.TB_PortDataServer.Name = "TB_PortDataServer";
            this.TB_PortDataServer.Size = new System.Drawing.Size(110, 20);
            this.TB_PortDataServer.TabIndex = 9;
            this.TB_PortDataServer.Text = "8887";
            this.TB_PortDataServer.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(640, 106);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(110, 49);
            this.button1.TabIndex = 8;
            this.button1.Text = "Отправить тестовое сообщение";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // B_ServerStart
            // 
            this.B_ServerStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.B_ServerStart.Location = new System.Drawing.Point(640, 22);
            this.B_ServerStart.Name = "B_ServerStart";
            this.B_ServerStart.Size = new System.Drawing.Size(110, 37);
            this.B_ServerStart.TabIndex = 7;
            this.B_ServerStart.Text = "Старт Сервер";
            this.B_ServerStart.UseVisualStyleBackColor = true;
            this.B_ServerStart.Click += new System.EventHandler(this.B_ServerStart_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(291, -5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 24);
            this.label1.TabIndex = 6;
            this.label1.Text = "Логи";
            // 
            // RTB_Logs
            // 
            this.RTB_Logs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RTB_Logs.Location = new System.Drawing.Point(10, 22);
            this.RTB_Logs.Name = "RTB_Logs";
            this.RTB_Logs.Size = new System.Drawing.Size(623, 391);
            this.RTB_Logs.TabIndex = 5;
            this.RTB_Logs.Text = "";
            // 
            // TB_MasterServer
            // 
            this.TB_MasterServer.Controls.Add(this.button3);
            this.TB_MasterServer.Controls.Add(this.textBox2);
            this.TB_MasterServer.Controls.Add(this.label3);
            this.TB_MasterServer.Controls.Add(this.B_RunGameServer);
            this.TB_MasterServer.Controls.Add(this.L_CountRunServer);
            this.TB_MasterServer.Controls.Add(this.RTB_LogsMS);
            this.TB_MasterServer.Location = new System.Drawing.Point(4, 22);
            this.TB_MasterServer.Name = "TB_MasterServer";
            this.TB_MasterServer.Padding = new System.Windows.Forms.Padding(3);
            this.TB_MasterServer.Size = new System.Drawing.Size(761, 418);
            this.TB_MasterServer.TabIndex = 1;
            this.TB_MasterServer.Text = "Master-Server";
            this.TB_MasterServer.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(627, 56);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(120, 20);
            this.textBox2.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(671, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Port";
            // 
            // B_RunGameServer
            // 
            this.B_RunGameServer.Location = new System.Drawing.Point(626, 6);
            this.B_RunGameServer.Name = "B_RunGameServer";
            this.B_RunGameServer.Size = new System.Drawing.Size(121, 23);
            this.B_RunGameServer.TabIndex = 7;
            this.B_RunGameServer.Text = "Запустить сервер";
            this.B_RunGameServer.UseVisualStyleBackColor = true;
            this.B_RunGameServer.Click += new System.EventHandler(this.B_RunGameServer_Click);
            // 
            // L_CountRunServer
            // 
            this.L_CountRunServer.AutoSize = true;
            this.L_CountRunServer.Location = new System.Drawing.Point(7, 286);
            this.L_CountRunServer.Name = "L_CountRunServer";
            this.L_CountRunServer.Size = new System.Drawing.Size(170, 13);
            this.L_CountRunServer.TabIndex = 6;
            this.L_CountRunServer.Text = "Кол-во запущенных серверов: 0";
            // 
            // RTB_LogsMS
            // 
            this.RTB_LogsMS.Location = new System.Drawing.Point(7, 6);
            this.RTB_LogsMS.Name = "RTB_LogsMS";
            this.RTB_LogsMS.Size = new System.Drawing.Size(613, 273);
            this.RTB_LogsMS.TabIndex = 5;
            this.RTB_LogsMS.Text = "";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(627, 83);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(120, 40);
            this.button3.TabIndex = 10;
            this.button3.Text = "Выбрать карту запуска";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(765, 439);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.TB_DataServer.ResumeLayout(false);
            this.TB_DataServer.PerformLayout();
            this.TB_MasterServer.ResumeLayout(false);
            this.TB_MasterServer.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage TB_DataServer;
        public System.Windows.Forms.TextBox TB_PortDataServer;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button B_ServerStart;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.RichTextBox RTB_Logs;
        private System.Windows.Forms.TabPage TB_MasterServer;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button B_RunGameServer;
        private System.Windows.Forms.Label L_CountRunServer;
        public System.Windows.Forms.RichTextBox RTB_LogsMS;
        private System.Windows.Forms.Button B_SaveLogs;
        private System.Windows.Forms.Button B_ClearLogs;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button3;
    }
}

