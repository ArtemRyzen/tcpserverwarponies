﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
namespace TCPServerForWarPonies_WF
{
    class GameServerRun
    {
        static Process iStartProcess;

        private int GlobalIdProcess = 0;

        private int PortServer = 0;

        public void RunServer(int port)
        {
            PortServer = port;
            iStartProcess = new Process(); // новый процесс
            iStartProcess.StartInfo.FileName = @"E:\new folder\for Games\ForWarPoniesFiles\Tools\FTPServer\WarPonies\MultiplayerShooter\Binaries\Win64\MultiplayerShooterServer.exe"; // путь к запускаемому файлу
            iStartProcess.StartInfo.Arguments = "-log -port=" + port; // эта строка указывается, если программа запускается с параметрами (здесь указан пример, для наглядности)
            Program.f1.AddingRecordsFromRTB_LogsMS(string.Format("Запущен сервер: {0}:{1}\n", "26.69.4.155", port));
            iStartProcess.StartInfo.WindowStyle = ProcessWindowStyle.Normal; // эту строку указываем, если хотим запустить программу в скрытом виде
            iStartProcess.EnableRaisingEvents = true;
            iStartProcess.Exited += new EventHandler(IStartProcess_Exited);
            iStartProcess.Start(); // запускаем программу
            GlobalIdProcess = iStartProcess.Id;
            Program.f1.AddingRecordsFromRTB_LogsMS(string.Format("Глобальный ID процесса: {0}", iStartProcess.Id));
        }

        public void StopServer()
        {
            iStartProcess.Kill();
        }

        public void IStartProcess_Exited(object sender, EventArgs e)
        {
            //Process process = (Process)sender;
            Program.f1.AddingRecordsFromRTB_LogsMS(string.Format("Сервер выключен: Port {0}",PortServer));
        }
    }

}
